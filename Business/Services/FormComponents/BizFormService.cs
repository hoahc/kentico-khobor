﻿using Business.ViewModels;
using CMS.ContactManagement;
using CMS.DataEngine;
using CMS.OnlineForms;
using Kentico.Forms.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Business.Services.FormComponents
{
    public class BizFormService : BaseService, IBizFormService
    {
        protected IFormProvider FormProvider { get; }
        protected IFormComponentModelBinder FormComponentModelBinder { get; }
        protected IFormComponentVisibilityEvaluator FormComponentVisibilityEvaluator { get; }

        public BizFormService(
            IFormProvider formProvider,
            IFormComponentModelBinder formComponentModelBinder,
            IFormComponentVisibilityEvaluator formComponentVisibilityEvaluator)
        {
            FormProvider = formProvider ?? throw new ArgumentNullException(nameof(formProvider));
            FormComponentModelBinder = formComponentModelBinder ?? throw new ArgumentNullException(nameof(formComponentModelBinder));
            FormComponentVisibilityEvaluator = formComponentVisibilityEvaluator ?? throw new ArgumentNullException(nameof(formComponentVisibilityEvaluator));
        }

        public FormComponentViewModel GetBizFormVM(string formName, int siteID)
        {
            GetBizFormVM(formName, siteID, out List<FormComponent> formComponents, out FormBuilderConfiguration formBuilderConfiguration);

            return new FormComponentViewModel()
            {
                FormComponents = formComponents,
                FormConfiguration = formBuilderConfiguration
            };
        }

        public FormComponentViewModel GetBizFormVM(string formName, int siteID, string elementId, ControllerContext controllerContext)
        {
            GetBizFormVM(formName, siteID, out List<FormComponent> formComponents, out FormBuilderConfiguration formBuilderConfiguration, elementId, controllerContext);

            // Build/Rebuild your form partial's view model with the submitted data included.
            return new FormComponentViewModel()
            {
                ElementId = elementId,
                FormComponents = formComponents,
                FormConfiguration = formBuilderConfiguration
            };
        }

        private void GetBizFormVM(
            string formName, 
            int siteID, 
            out List<FormComponent> formComponents, 
            out FormBuilderConfiguration formBuilderConfiguration, 
            string elementId = null, 
            ControllerContext controllerContext = null)
        {
            var formInfo = BizFormInfoProvider.GetBizFormInfo(formName, siteID);

            var className = DataClassInfoProvider.GetClassName(formInfo.FormClassID);

            BizFormItem existingBizFormItem = null;
            if (className != null)
            {
                var items = BizFormItemProvider.GetItems(className);
                existingBizFormItem = items?.GetExistingItemForContact(formInfo, ContactManagementContext.CurrentContact?.ContactGUID);
            }

            if (!string.IsNullOrEmpty(elementId) || controllerContext != null)
            {
                // Use the wrapping element ID for the form as the prefix in the model binder
                // to ensure that the form gets bound correctly.
                var modelBinder = new FormBuilderModelBinder(formInfo, FormProvider, FormComponentModelBinder, FormComponentVisibilityEvaluator, elementId);
                var modelBindingContext = new FormBuilderModelBindingContext()
                {
                    Contact = ContactManagementContext.CurrentContact,
                    ExistingItem = existingBizFormItem
                };

                formComponents = modelBinder.BindModel(controllerContext, modelBindingContext) as List<FormComponent>;
            }
            else formComponents = FormProvider.GetFormComponents(formInfo)
                                              .GetDisplayedComponents(ContactManagementContext.CurrentContact, formInfo, existingBizFormItem, null) as List<FormComponent>;

            var formDeserializationSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                TypeNameHandling = TypeNameHandling.Auto,
                StringEscapeHandling = StringEscapeHandling.EscapeHtml
            };

            formBuilderConfiguration = JsonConvert.DeserializeObject<FormBuilderConfiguration>(formInfo.FormBuilderLayout, formDeserializationSettings);
        }
    }
}
