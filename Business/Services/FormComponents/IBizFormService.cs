﻿using Business.Dto;
using Business.ViewModels;
using System.Web.Mvc;

namespace Business.Services.FormComponents
{
    public interface IBizFormService : IService
    {
        FormComponentViewModel GetBizFormVM(string formName, int siteID);
        FormComponentViewModel GetBizFormVM(string formName, int siteID, string elementId, ControllerContext controllerContext);
    }
}
