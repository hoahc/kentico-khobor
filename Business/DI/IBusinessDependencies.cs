﻿using Business.Services.Context;

namespace Business.DI
{
    public interface IBusinessDependencies
    {
        ISiteContextService SiteContextService { get; }
    }
}
