﻿using Business.Services.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.DI
{
    public class BusinessDependencies : IBusinessDependencies
    {
        public ISiteContextService SiteContextService { get; }

        public BusinessDependencies(ISiteContextService siteContextService)
        {
            SiteContextService = siteContextService;
        }
    }
}
