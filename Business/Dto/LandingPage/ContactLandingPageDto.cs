﻿using CMS.DocumentEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Dto.LandingPage
{
    public class ContactLandingPageDto : LandingPageDto
    {
        public DateTime ContactDate { get; set; }

        public static Func<DocumentQuery<CMS.DocumentEngine.Types.Khobor.ContactLandingPage>, DocumentQuery<CMS.DocumentEngine.Types.Khobor.ContactLandingPage>> QueryModifier =
               (originalQuery) =>
                    originalQuery.AddColumns("ContactDate");

        public static Func<CMS.DocumentEngine.Types.Khobor.ContactLandingPage, ContactLandingPageDto, ContactLandingPageDto> Selector =
            (landingPage, landingPageDto) =>
            {
                landingPageDto.ContactDate = landingPage.ContactDate;

                return landingPageDto;
            };
    }
}
