﻿using Kentico.Forms.Web.Mvc;
using System.Collections.Generic;

namespace Business.ViewModels
{
    public class FormComponentViewModel : IViewModel
    {
        public FormBuilderConfiguration FormConfiguration { get; set; }

        public List<FormComponent> FormComponents { get; set; }

        public string ElementId { get; set; }
    }
}