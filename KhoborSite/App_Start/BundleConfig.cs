using System.Web;
using System.Web.Optimization;

namespace KhoborSite
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Custom JavaScript files from the ~/Scripts/ directory can be included as well
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Content/FormComponents/Select2Dropdown/select2-dropdown-component.js",
                        "~/Scripts/site.js"));

            // Custom CSS files from the ~/Content/ directory can be included as well
            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/FormComponents/Select2Dropdown/Select2DropdownComponent.css",
                        "~/Content/site.css"));
        }
    }
}