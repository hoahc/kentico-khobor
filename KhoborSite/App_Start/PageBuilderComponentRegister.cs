﻿using Kentico.PageBuilder.Web.Mvc;

[assembly: RegisterSection(
    "Khobor.Section.SingleColumn",
    "{$Section.SingleColumn.Name$}",
    customViewName: "Sections/_SingleColumnSection",
    Description = "{$Section.SingleColumn.Description$}",
    IconClass = "icon-square")]