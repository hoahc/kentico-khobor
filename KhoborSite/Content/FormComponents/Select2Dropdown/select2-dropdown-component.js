﻿window.khobor = window.khobor || {};

(function (select2DropdownComponent) {
    function loadScript(url, callback) {
        // Adding the script tag to the head as suggested before
        var head = document.head;
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;

        // Then bind the event to the callback function.
        // There are several events for cross browser compatibility.
        script.onreadystatechange = callback;
        script.onload = callback;

        // Fire the loading
        head.appendChild(script);
    }

    function getResultsXml(term) {
        return '<?xml version="1.0" encoding="ISO-8859-1"?>'
            + '<CATALOG>'
            + '<CD>'
            + '<id>1</id>'
            + '<text>Bob Dylan</text>'
            + '</CD>'
            + '<CD>'
            + '<id>2</id>'
            + '<text>Bonnie Tyler</text>'
            + '</CD>'
            + '</CATALOG>';
    }

    select2DropdownComponent.onselectchange = function (target) {
        console.log('onselectchange', target);
    };

    select2DropdownComponent.init = function (target) {
        console.log(target);

        loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.js', function () {
            console.log('init: load success :' + target);
            console.log(jQuery);

            console.log($('#' + target));
            var select2Selector = $('#' + target).select2("destroy");
            var serviceURL = select2Selector.attr('data-service-url');
            var queryParameterName = select2Selector.attr('data-query-parameter');

            select2Selector.select2({
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: 'Select...',
                ajax: {
                    url: serviceURL,
                    dataType: 'xml',
                    delay: 250,
                    data: function (term) {
                        return {
                            postalcode: term // search term
                        };
                    },
                    results: function (data, params) {
                        return {
                            results: $(data).children('geonames').children('code').map(function () {
                                var $cd = $(this);
                                var value = [$cd.children('postalcode').text(), $cd.children('name').text(), $cd.children('countryCode').text()].join(';');
                                var text = ['[', $cd.children('postalcode').text(), '] ', $cd.children('name').text(), ' (', $cd.children('countryCode').text(), ')'].join('');

                                return {
                                    id: value,
                                    text: text
                                };
                            })
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1
            });
        });
    };
}(window.khobor.select2DropdownComponent = window.khobor.select2DropdownComponent || {}));