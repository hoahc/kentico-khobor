﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KhoborSite.Config
{
    public class AppConfig
    {
        public const string Sitename = "Khobor";
        public const string ContentDirectory = "~/Content";
    }
}