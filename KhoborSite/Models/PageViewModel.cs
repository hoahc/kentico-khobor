﻿using Business.DI;
using Business.Dto.Page;

namespace KhoborSite.Models
{
    /// <summary>
    /// Base class 
    /// </summary>
    public class PageViewModel : IViewModel
    {
        public PageMetadataDto Metadata { get; set; }
        public UserMessage UserMessage { get; set; }

        public static PageViewModel GetPageViewModel(
            string title,
            IBusinessDependencies dependencies,
            string message = null,
            bool displayAsRaw = false,
            MessageType messageType = MessageType.Info) =>
            new PageViewModel()
            {
                Metadata = GetPageMetadata(title, dependencies),
                UserMessage = new UserMessage
                {
                    Message = message,
                    MessageType = messageType,
                    DisplayAsRaw = displayAsRaw
                }
            };

        protected static PageMetadataDto GetPageMetadata(string title, IBusinessDependencies dependencies) =>
            new PageMetadataDto()
            {
                Title = title,
                CompanyName = dependencies.SiteContextService.SiteName
            };
    }

    public class PageViewModel<TViewModel> : PageViewModel where TViewModel : IViewModel
    {
        public TViewModel Data { get; set; }

        public static PageViewModel<TViewModel> GetPageViewModel(
            TViewModel data,
            string title,
            IBusinessDependencies dependencies,
            string message = null,
            bool displayAsRaw = false,
            MessageType messageType = MessageType.Info) =>
            new PageViewModel<TViewModel>()
            {
                Metadata = GetPageMetadata(title, dependencies),
                UserMessage = new UserMessage
                {
                    Message = message,
                    MessageType = messageType,
                    DisplayAsRaw = displayAsRaw
                },
                Data = data
            };
    }
}