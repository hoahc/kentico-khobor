﻿using CMS.DataEngine;
using Kentico.Forms.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace KhoborSite.Models.FormComponents
{
    public class Select2DropdownProperties : FormComponentProperties<string>
    {
        public Select2DropdownProperties() : base(FieldDataType.Text) { }

        [DefaultValueEditingComponent(TextInputComponent.IDENTIFIER)]
        public override string DefaultValue { get; set; } = string.Empty;

        // Assigns the 'TextInputComponent' as the editing component of the 'QueryParameterName' property
        [EditingComponent(TextInputComponent.IDENTIFIER,
            Label = "{$FormComponent.Select2Dropdown.QueryParameterName.Name$}",
            Tooltip = "{$FormComponent.Select2Dropdown.QueryParameterName.Tooltip$}",
            ExplanationText = "{$FormComponent.Select2Dropdown.QueryParameterName.ExplanationText$}",
            Order = 0)]
        [Required]
        public string QueryParameterName { get; set; }

        // Assigns the 'TextInputComponent' as the editing component of the 'ServiceURL' property
        [EditingComponent(TextInputComponent.IDENTIFIER,
            Label = "{$FormComponent.Select2Dropdown.ServiceURL.Name$}",
            Tooltip = "{$FormComponent.Select2Dropdown.ServiceURL.Tooltip$}",
            ExplanationText = "{$FormComponent.Select2Dropdown.ServiceURL.ExplanationText$}",
            Order = 0)]
        [Required]
        public string ServiceURL { get; set; }
    }
}