﻿using Kentico.Forms.Web.Mvc;
using KhoborSite.Models.FormComponents;

[assembly: RegisterFormComponent(
    "KhoborSite.FormComponent.Select2Dropdown",
    typeof(Select2DropdownComponent),
    "{$FormComponent.Select2Dropdown.Name$}",
    ViewName = "FormComponents/_Select2Dropdown",
    Description = "{$FormComponent.Select2Dropdown.Description$}",
    IconClass = "icon-picture")]

namespace KhoborSite.Models.FormComponents
{
    public class Select2DropdownComponent : FormComponent<Select2DropdownProperties, string>
    {
        public Select2DropdownComponent()
        {
        }

        [BindableProperty]
        public string SelectedValue { get; set; }

        public override string GetValue() => SelectedValue;

        public override void SetValue(string value)
        {
            SelectedValue = value;
        }
    }
}