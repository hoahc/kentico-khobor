﻿using Business.Repository.Forms;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KhoborSite.Models.Forms
{
    public class ContactRegistrationViewModel : BaseFormViewModel, IFormViewModel, IViewModel
    {
        protected const string RequiredFieldErrorMessage = "This field is required.";
        protected const string TextFieldLengthErrorMessage = "The first name mustn't exceed 200 characters.";

        public IDictionary<string, object> Fields =>
            GetFields(
                new KeyValuePair<string, object>(nameof(Name), Name),
                new KeyValuePair<string, object>(nameof(Address), Address)
            );

        [Display(Name = "Name", Prompt = "Enter your full name here")]
        [Required(ErrorMessage = RequiredFieldErrorMessage)]
        [MaxLength(200, ErrorMessage = TextFieldLengthErrorMessage)]
        public string Name { get; set; }

        [Display(Name = "Address", Prompt = "Enter your address here")]
        [Required(ErrorMessage = RequiredFieldErrorMessage)]
        [MaxLength(200, ErrorMessage = TextFieldLengthErrorMessage)]
        public string Address { get; set; }
    }
}