﻿namespace KhoborSite.Models
{
    public enum MessageType
    {
        Info,
        Warning,
        Error
    }
}