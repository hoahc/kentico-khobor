using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Kentico.OnlineMarketing.Web.Mvc;
using Kentico.Web.Mvc;
using KhoborSite.App_Start;

namespace KhoborSite
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Enables and configures selected Kentico ASP.NET MVC integration features
            ApplicationConfig.RegisterFeatures(ApplicationBuilder.Current);

            //// Gets the ApplicationBuilder instance
            //// Allows you to enable and configure selected Kentico MVC integration features
            //ApplicationBuilder builder = ApplicationBuilder.Current;

            //// Enables the A/B testing feature, which provides system routes used to log A/B testing data and conversions
            //builder.UseABTesting();

            // Dependency injection
            AutofacConfig.ConfigureContainer();

            // Registers routes including system routes for enabled features
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            // Registers enabled bundles
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
