﻿using Business.DI;
using KhoborSite.Models;
using System.Web.Mvc;

namespace KhoborSite.Controllers
{
    public class BaseController : Controller
    {
        protected IBusinessDependencies Dependencies { get; }

        protected BaseController(IBusinessDependencies dependencies)
        {
            Dependencies = dependencies;
        }
    }
}