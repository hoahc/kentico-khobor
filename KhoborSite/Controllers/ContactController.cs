﻿using Business.DI;
using Business.Services.FormComponents;
using Business.ViewModels;
using CMS.CustomTables;
using CMS.DataEngine;
using CMS.SiteProvider;
using System;
using System.Linq;
using System.Web.Mvc;

namespace KhoborSite.Controllers
{
    public class ContactController : BaseController
    {
        protected const string FormName = "ContactRegistration";
        protected const string TableName = "customtable.contact";
        protected IBizFormService BizFormService { get; }

        public ContactController(
            IBusinessDependencies dependencies,
            IBizFormService bizFormService) : base(dependencies)
        {
            BizFormService = bizFormService ?? throw new ArgumentNullException(nameof(bizFormService));
        }

        // GET: Contact
        public ActionResult Index()
        {
            return View(BizFormService.GetBizFormVM(FormName, SiteContext.CurrentSiteID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(FormComponentViewModel vm)
        {
            var model = BizFormService.GetBizFormVM(FormName, SiteContext.CurrentSiteID, vm.ElementId, ControllerContext);

            // Save the form data
            if (model != null && ModelState.IsValid)
            {
                var name = model.FormComponents.FirstOrDefault(c => c.Name == "Name")?.GetObjectValue() + string.Empty;
                var address = model.FormComponents.FirstOrDefault(c => c.Name == "Address")?.GetObjectValue() + string.Empty;

                var items = address.Split(';');
                var postalCode = items.Length > 0 ? items[0] : string.Empty;
                var geoName = items.Length > 1 ? items[1] : string.Empty;
                var countryCode = items.Length > 2 ? items[2] : string.Empty;

                // Gets the custom table
                DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(TableName);
                if (customTable != null)
                {
                    // Creates a new custom table item
                    CustomTableItem newCustomTableItem = CustomTableItem.New(TableName);

                    // Sets the values for the fields of the custom table (ItemText in this case)
                    newCustomTableItem.SetValue("Name", name);
                    newCustomTableItem.SetValue("PostalCode", postalCode);
                    newCustomTableItem.SetValue("GeoName", geoName);
                    newCustomTableItem.SetValue("CountryCode", countryCode);

                    // Save the new custom table record into the database
                    newCustomTableItem.Insert();

                    model.FormComponents.ForEach(c => c.SetObjectValue(null));
                }
            }

            return View(BizFormService.GetBizFormVM(FormName, SiteContext.CurrentSiteID));
        }
    }
}